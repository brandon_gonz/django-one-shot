from django.shortcuts import redirect, render, get_object_or_404
from .models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoItemForm

# Create your views here.
def todo_list(request):
    todo_lists = TodoList.objects.all()
    context = {
        "todo_lists": todo_lists
    }
    return render(request, "todos/list.html", context)

def todo_list_detail(request, id):
  todo_list = get_object_or_404(TodoList, id=id)
  context = {
    "todo_list": todo_list
  }
  return render(request, "todos/detail.html", context)

def todo_list_create(request):
  if request.method == "POST":
    form = TodoListForm(request.POST)
    if form.is_valid():
      # To redirect to the detail view of the model, use this:
      todo_list = form.save()
      return redirect("todo_list_detail", id=todo_list.id)
  else:
    form = TodoListForm()

  context = {
    "form": form
  }

  return render(request, "todos/create.html", context)

def todo_list_edit(request, id):
  todo_list = TodoList.objects.get(id=id)
  if request.method == "POST":
    form = TodoListForm(request.POST, instance=todo_list)
    if form.is_valid():
      form.save()
      return redirect("todo_list_detail", id=id)
  else:
    form = TodoListForm(instance=todo_list)
    context = {
      "name": "name"
    }
    return render(request, "todos/edit.html", context)

def todo_list_delete(request, id):
  todo_list = TodoList.objects.get(id=id)
  if request.method == "POST":
    todo_list.delete()
    return redirect("todo_list_list")

  return render(request, "todos/delete.html")


def todo_item_create(request):
  print("HELLO")
  if request.method == "POST":
    print("World")
    form = TodoItemForm(request.POST)
    if form.is_valid():
      # To redirect to the detail view of the model, use this:
      todo_item = form.save()

      return redirect("todos.detail.html", id=todo_item.list.id)


  else:
    form = TodoItemForm()

  context = {
    "form": "form"
  }

  return render(request, "todos/create_item.html", context)
